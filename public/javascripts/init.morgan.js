require.config({
	baseUrl: "javascripts",
	paths: {
		jquery     : "libs/jquery-2.1.0.min",
		bootstrap  : "libs/bootstrap.min",
		underscore : "libs/underscore-min",
		backbone   : "libs/backbone-min",
    text       : "libs/text",
    morgan : "morgan",
    StoreItemModel: "models/StoreItemModel",
    TopNavigationView : "navigation/TopNavigationView",
    MainLayoutView : "layouts/MainLayoutView",
    ProductBrowserView: "productbrowser/ProductBrowserView",
    AvailableProducts: "productbrowser/AvailableProductsMock",
    ShoppingCartCollection: "cart/ShoppingCartCollection",
    ShoppingCartSideView: "cart/ShoppingCartSideView",
    ShoppingCartMainView: "cart/ShoppingCartMainView",
    authMock: "auth/AuthenticationServiceMock",
    LoginDialogueView: "auth/LoginDialogueView"
	},
	shim: {
		jquery: {
			exports: "$"
		},
		bootstrap : {
      deps: ['jquery']
    },
    backbone : {
      deps : ['jquery', 'underscore'],
      exports : 'Backbone'
    },
    underscore: {
     exports: "_"
   },
   datepicker: {
     deps: ['jquery'],
     exports: ""
   }
 }
});

require(['morgan'], 
  function(morgan){

      morgan.start();

  }
);