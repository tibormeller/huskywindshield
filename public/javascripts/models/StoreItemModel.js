define(['backbone'],
	function(Backbone){

		'use strict';
    return Backbone.Model.extend({
        defaults: {
          id: 0,
          name: 'Store Item',
          imageUrl: 'images/products/placeholder.jpg',
          desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
          price: 42,
          quantity: 10
        }
      });
  });