define(['backbone',
  'TopNavigationView', 'MainLayoutView', 'ProductBrowserView', 'AvailableProducts', 
  'ShoppingCartCollection', 'ShoppingCartSideView', 'ShoppingCartMainView',
  'authMock', 'LoginDialogueView'],
function (Backbone, 
  TopNavigationView, MainLayoutView, ProductBrowserView, AvailableProducts, 
  ShoppingCartCollection, ShoppingCartSideView, ShoppingCartMainView,
  authMock, LoginDialogueView
) {

  'use strict';

  var initAppAndStart = function () {

    var Router = Backbone.Router.extend({
      routes: {
        '': 'home',
        'cart': 'cart',
        'login': 'login'
      },
      initialize: function () {
      }
    });

    new TopNavigationView();
    new MainLayoutView();

    var shoppingCart = new ShoppingCartCollection();
    var sideView = new ShoppingCartSideView({ collection: shoppingCart });

    var productBrowser = new ProductBrowserView({
        collection: new AvailableProducts(),
        cart: shoppingCart 
      });

    var cartView = new ShoppingCartMainView({
      collection: shoppingCart
    });

    var router = new Router();
    router.on('route:home', function () {
      productBrowser.render();
      sideView.render()
    });

    router.on('route:cart', function () {
      cartView.render();
    });

    router.on('route:login', function () {
      if (!authMock.isUserLoggedIn()) {
        var login = new LoginDialogueView({
          router: router
        });
        login.render();
      } else {
        router.navigate('', {trigger: true, replace: true});
      }
      
    });

    Backbone.history.start();

  };

  return {
    start: function () {
      initAppAndStart();
    }

  };

});