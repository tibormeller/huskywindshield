define(['jquery', 'underscore', 'backbone',
  'text!auth/LoginDialogueTemplate.html',
  'authMock'
  ],
  function($, _, Backbone, loginTemplate, auth){

    'use strict';
    return Backbone.View.extend({

      el: '#main',
      events: {
        'click #submitLogin': 'onLoginSubmit'
      },
      
      router: undefined,
      
      render: function () { 
        this.$el.html(this.template);
      },
      
      initialize: function (options) {
        this.router = options.router;
        this.template = _.template(loginTemplate);
      },

      onLoginSubmit: function () {
        auth.login($('#user').val(), $('#pass').val());
        this.router.navigate('', {trigger: true, replace: true});
      }

    });

 });
