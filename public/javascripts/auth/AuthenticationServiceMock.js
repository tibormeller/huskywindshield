define([],
function () {

  'use strict';

  var username, password;

  return {
    
    login: function (user, password) {
      username = user;
    },

    logout: function () {
      username = null;
    },

    isUserLoggedIn: function () {
      return (username) ? true : false;
    }

  };

});