define(['jquery', 'underscore', 'backbone',
  'text!layouts/MainLayoutTemplate.html'
  ],
  function($, _, Backbone, mainLayoutTemplate){

    'use strict';
    return Backbone.View.extend({

      el: 'body',
      template: _.template(mainLayoutTemplate),
      
      render: function () {
        this.$el.append(this.template);
      },
      
      initialize: function () {
        this.render();
      }
    });

 });
