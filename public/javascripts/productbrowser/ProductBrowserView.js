define(['jquery', 'underscore', 'backbone',
  'text!productbrowser/ProductBrowserTemplate.html', 'authMock'
  ],
  function($, _, Backbone, productBrowserTemplate, authMock){

    'use strict';
    return Backbone.View.extend({

      el: '#main',
      events: {
        'click .btn.btn-add' : 'itemToCart'
      },

      cart: undefined,
      currency: "USD",
      
      render: function () {
        this.template = _.template(productBrowserTemplate, {
          products: this.collection.models,
          currency: this.currency,
          isUserLoggedIn: authMock.isUserLoggedIn()
        });

        this.$el.html(this.template);
      },
      
      initialize: function (options) {
        this.cart = options.cart;
      },

      itemToCart: function (event) {
        var prodId = $(event.currentTarget).attr('data-id');
        console.log('Adding ' + prodId + 'to cart.');
        var itemToAdd = this.collection.get(prodId);

        this.cart.add(itemToAdd);
      }

    });

 });
