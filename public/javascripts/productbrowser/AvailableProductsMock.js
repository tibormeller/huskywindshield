define(['backbone', 'StoreItemModel'],
  function(Backbone, StoreItemModel){

    'use strict';
    return Backbone.Collection.extend({
      
      model: StoreItemModel,

      initialize: function () {
        this.generateMockItems();
      },

      generateMockItems: function () {

        this.add(new StoreItemModel({
          id: 1,
          name: 'Old Horse',
          imageUrl: 'images/products/old-horse.jpg',
          desc: 'A bit old but his first owner relly loved him.',
          price: 67,
          quantity: 6
        }));

        this.add(new StoreItemModel({
          id: 2,
          name: 'Some cheep chinese stuff',
          imageUrl: 'images/products/chinees-stuff.jpg',
          desc: 'It is cheep and we have a huge amount of it in our warehouse.',
          price: 12,
          quantity: 103
        }));

        this.add(new StoreItemModel({
          id: 3,
          name: 'Ruber duck',
          imageUrl: 'images/products/ruber-duck.jpg',
          desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
          price: 73,
          quantity: 34
        }));

        this.add(new StoreItemModel({
          id: 1,
          name: 'Used Car',
          imageUrl: 'images/products/placeholder.jpg',
          desc: 'It can bring you anywhere. And there is just a few kilomter in it.',
          price: 230,
          quantity: 12
        }));

        this.add(new StoreItemModel());
      }
    });

  });
