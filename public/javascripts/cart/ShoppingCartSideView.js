define(['jquery', 'underscore', 'backbone',
  'text!cart/ShoppingCartSideTemplate.html', 'authMock'
  ],
  function($, _, Backbone, sideTemplate, authMock){

    'use strict';
    return Backbone.View.extend({

      el: '#side',
      
      render: function () {

        if (!authMock.isUserLoggedIn()) return;

        this.template = _.template(sideTemplate, {
          cartItemNo: this.collection.models.length,
          total: this.getTotal()
        });        
        this.$el.html(this.template);
      },
      
      initialize: function (options) {
        this.collection.on('add', this.onItemAdded, this);
      },

      getTotal: function () {
        var total = 0;
        var models = this.collection.models;
        for (var i=0;i< models.length; i++)
        {
          total += models[i].get('price');
        }
        return total;
      },

      onItemAdded: function (event) {
        this.render();
      }

    });

 });
