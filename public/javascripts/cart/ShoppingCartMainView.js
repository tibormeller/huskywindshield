define(['jquery', 'underscore', 'backbone',
  'text!cart/ShoppingCartMainTemplate.html'
  ],
  function($, _, Backbone, shoppingCartMainTemplate){

    'use strict';
    return Backbone.View.extend({

      el: '#main',

      currency: "USD",
      
      render: function () {
        this.template = _.template(shoppingCartMainTemplate, {
          products: this.collection.models,
          currency: this.currency
        });
        this.$el.html(this.template);
      },
      
      initialize: function (options) {
        
      }

    });

 });
