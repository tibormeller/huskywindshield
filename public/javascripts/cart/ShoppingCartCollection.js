define(['backbone', 'StoreItemModel'],
  function(Backbone, StoreItemModel){

    'use strict';
    return Backbone.Collection.extend({
      model: StoreItemModel
    });
    
  });
