define(['jquery', 'underscore', 'backbone',
  'text!navigation/TopNavigationTemplate.html'
  ], 
  function($, _, backbone, topNavTemplate){

   return Backbone.View.extend({
    
     el: 'body',
     template: _.template(topNavTemplate, {title: 'Husky Windshild Store'}),
     
     render: function () {
       this.$el.append(this.template);
     },

     initialize: function () {
       this.render();
     }
   });

 });
